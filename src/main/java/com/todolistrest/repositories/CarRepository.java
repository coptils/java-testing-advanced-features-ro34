package com.todolistrest.repositories;


import com.todolistrest.entities.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    List<Car> findAllByColor(String color);
    Car findByRegistryNumber(String registryNumber);

}
