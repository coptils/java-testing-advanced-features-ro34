package com.todolistrest.services;

import com.todolistrest.dto.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    private DeliveryPlatform platform;

    public void send(String to, String subject, String body, boolean html) {
        String format = "text";
        if (html) {
            format = "html";
        }

        Email email = new Email(to, subject, body);
        email.setFormat(format);
        platform.deliver(email);
    }
}