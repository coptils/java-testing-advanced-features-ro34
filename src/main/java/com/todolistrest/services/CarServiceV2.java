package com.todolistrest.services;

import com.todolistrest.entities.Car;
import com.todolistrest.exceptions.ResourceNotFoundException;
import com.todolistrest.repositories.CarRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceV2 {

    private final CarRepository carRepository;

    public CarServiceV2(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public Car save(Car car) {
        return carRepository.save(car);
    }

    public List<Car> findAllByColor(String color) {
        List<Car> carList = carRepository.findAllByColor(color);
        return carList;
    }

    public Car findAllByRegistryNumber(String registryNumber) {
        Car car = carRepository.findByRegistryNumber(registryNumber);
        if (car == null) {
            throw new ResourceNotFoundException("Does not exist");
        }
        return car;
    }

    public void deleteCar(String registryNumber) {
        carRepository.delete(findAllByRegistryNumber(registryNumber));
    }

    public void deteleCarsByColor(String color) {
        carRepository.deleteAllInBatch(findAllByColor(color));
    }


}
