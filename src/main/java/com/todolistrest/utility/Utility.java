package com.todolistrest.utility;

import com.todolistrest.dto.CurrencyConverter;

public class Utility {

    private Utility() {
    }

    public static boolean isValidEmail(String email) {
        if (email == null || email.isBlank() || !email.contains("@")) {
            return false;
        }

        return email.contains(".com") || email.contains(".ro") || email.contains(".fr");
    }

    public static double convertCurrency(CurrencyConverter converter, double money) {
        if (converter == CurrencyConverter.USD) {
            return money * 4.92;
        }

        if (converter == CurrencyConverter.EUR) {
            return money * 4.95;
        }

        if (converter == CurrencyConverter.RON) {
            return money;
        }

        return 0;
    }

    public static boolean isOdd(int number) {
        return number % 2 != 0;
    }

    public static int add(int x, int y) {
        return x + y;
    }

    public static int multiply(int x, int y) {
        return x * y;
    }

}
