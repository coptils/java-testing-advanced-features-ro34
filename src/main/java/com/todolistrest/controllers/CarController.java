package com.todolistrest.controllers;

import com.todolistrest.entities.Car;
import com.todolistrest.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {

    @Autowired
    private CarService carService;

    @PostMapping(value = "/cars")
    public Car save(@RequestBody Car car){
        return carService.save(car);
    }
    @GetMapping(value = "/cars")
    public List<Car> findAllByColor(@RequestParam String color){
        return carService.findAllByColor(color);
    }
    @GetMapping(value = "/cars/{registryNumber}")
    public Car findByRegistryNumber(@PathVariable String registryNumber){
        return carService.findAllByRegistryNumber(registryNumber);
    }
    @DeleteMapping(value = "/cars/{registryNumber}")
    public ResponseEntity delete(@PathVariable String registryNumber){
        carService.deleteCar(registryNumber);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    @DeleteMapping(value = "/cars")
    public ResponseEntity deleteAllByColor(@RequestParam String color){
        carService.deteleCarsByColor(color);
        return new  ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
