package com.todolistrest.dto;

public enum CurrencyConverter {
    USD, EUR, RON;
}
