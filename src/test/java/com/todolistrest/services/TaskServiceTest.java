package com.todolistrest.services;

import com.todolistrest.entities.Task;
import com.todolistrest.entities.User;
import com.todolistrest.exceptions.ResourceNotFoundException;
import com.todolistrest.repositories.TaskRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private UserService userService;

    @InjectMocks
    private TaskService taskService;

    @Test
    void shouldUpdate(){
        Task task= new Task();
        task.setName("task1");
        when(taskRepository.findById(anyLong()))
                .thenReturn(Optional.of(task));

        Task result = taskService.update(1L, task);

        verify(taskRepository, times(1)).save(any());
    }

    @Test
    void shouldNotUpdateWhenResourceNotFound(){
        when(taskRepository.findById(anyLong()))
                .thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException
                .class,()->taskService.update(1L, new Task()));
        verify(taskRepository, times(0)).save(any());
    }
}