package com.todolistrest.services;

import com.todolistrest.entities.Role;
import com.todolistrest.entities.RoleTypes;
import com.todolistrest.repositories.RoleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    @Mock
    private RoleRepository roleRepository;
    @InjectMocks
    private RoleService roleService;

    @Test
    void shouldNotSave_whenRoleFoundInDb() {
        Role role = buildRole();
        when(roleRepository.findRoleByName(any()))
                .thenReturn(role);

        RuntimeException result = assertThrows(RuntimeException.class,
                () -> roleService.save(role));
        assertThat(result).hasMessage("Role already exists");
        verifyNoMoreInteractions(roleRepository);
    }

    @Test
    void shouldSave_whenRoleNotFoundInDb() {
        when(roleRepository.findRoleByName(any()))
                .thenReturn(null);

        Role result = roleService.save(buildRole());

//        assertEquals(RoleTypes.USER, result.getName());
        verify(roleRepository, times(1))
                .save(any());
    }

    private Role buildRole() {
        Role role = new Role();
        role.setId(1L);
        role.setName(RoleTypes.USER);
        role.setDescription("my role user");
        return role;
    }
}