package com.todolistrest.services;

import com.todolistrest.entities.Car;
import com.todolistrest.exceptions.ResourceNotFoundException;
import com.todolistrest.repositories.CarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarServiceTest {

    @Mock
    private CarRepository carRepository;
    @InjectMocks
    private CarService carService;

//    @BeforeEach
//    void setUp() {
//        carService = new CarService();
//        carService.setCarRepository(carRepository);
//    }

    @Test
    void shouldSave() {
        when(carRepository.save(any())).thenThrow(RuntimeException.class);

        RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> carService.save(buildCar("red")));
        assertThat(runtimeException).hasMessage(null);
        assertThat(runtimeException).hasNoCause();
    }

    @Test
    void shouldNotFindCarByRegistryNumber() {
        when(carRepository.findByRegistryNumber(anyString()))
                .thenReturn(null);

        ResourceNotFoundException result =
                assertThrows(ResourceNotFoundException.class,
                        () -> carService.findAllByRegistryNumber("CJ10UAT"));

        assertThat(result).hasMessage("Does not exist");
    }

    @Test
    void shouldFindCarByRegistryNumber() {
        when(carRepository.findByRegistryNumber(anyString()))
                .thenReturn(buildCar("red"));

        Car result = carService.findAllByRegistryNumber("CJ10UAT");

        assertEquals("red", result.getColor());
    }

    @ParameterizedTest
    @ValueSource(strings = {"red", "blue", "black"})
    void shouldFindCarsByColor(String color) {
        when(carRepository.findAllByColor(color))
                .thenReturn(List.of(buildCar(color), buildCar(color)));

        List<Car> result = carService.findAllByColor(color);

        assertEquals(2, result.size());
        assertEquals(color, result.get(0).getColor());
    }

    @Test
    void shouldDeleteCar() {
        when(carRepository.findByRegistryNumber(anyString()))
                .thenReturn(buildCar("red"));

        carService.deleteCar("abcd");

        verify(carRepository, times(1)).delete(any());
    }

    @Test
    void shouldNotDeleteCarWhenResourceNotFound() {
        when(carRepository.findByRegistryNumber(anyString()))
                .thenReturn(null);

        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class, () -> carService.deleteCar("abcd"));

        verify(carRepository, times(0)).delete(any());
        assertThat(result).hasMessage("Does not exist");
    }

    private Car buildCar(String color) {
        Car car = new Car();
        car.setId(1L);
        car.setRegistryNumber("CJ10UAT");
        car.setColor(color);
        return car;
    }


}
