package com.todolistrest.services;

import com.todolistrest.entities.Car;
import com.todolistrest.repositories.CarRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CarServiceV2Test {

    private CarServiceV2 carService;

//    @BeforeEach
//    void setUp(@Mock CarRepository carRepository) {
//        carService = new CarServiceV2(carRepository);
//        when(carRepository.save(any()))
//                .thenReturn(buildCar());
//        when(carRepository.findAllByColor(anyString()))
//                .thenReturn(List.of(buildCar(), buildCar()));
//    }

//    @AfterEach
//    void tearDown() {
//
//    }
//
//    @BeforeAll
//    static void beforeAll() {
//    }
//
//    @AfterAll
//    static void afterAll() {
//    }

    @Test
    void shouldSaveCar(@Mock CarRepository carRepository) {
        carService = new CarServiceV2(carRepository);
        when(carRepository.save(any())).thenReturn(buildCar());

        Car car = buildCar();

        Car result = carService.save(car);

        assertEquals("red", result.getColor());
        assertEquals("CJ10UAT", result.getRegistryNumber());
    }

    @Test
    void shouldFindAllColors(@Mock CarRepository carRepository) {
        carService = new CarServiceV2(carRepository);
        when(carRepository.findAllByColor(anyString()))
                .thenReturn(List.of(buildCar(), buildCar()));
        Car car = buildCar();

        List<Car> result = carService.findAllByColor("red");

        assertEquals(2, result.size());
    }

    private Car buildCar() {
        Car car = new Car();
        car.setId(1L);
        car.setRegistryNumber("CJ10UAT");
        car.setColor("red");
        return car;
    }
}