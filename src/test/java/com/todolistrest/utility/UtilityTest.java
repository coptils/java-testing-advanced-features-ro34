package com.todolistrest.utility;

import com.todolistrest.dto.CurrencyConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.*;

class UtilityTest {


    @ParameterizedTest
    @ValueSource(strings = {"email@gmail.com", "email@gmail.ro", "email@gmail.fr"})
    void shouldReturnTrue_whenValidEmail(String email) {
        boolean result = Utility.isValidEmail(email);

        assertTrue(result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "    ", "null", "email@gmail.de", "email@gmail.uk", "jsnsw.com"})
    void shouldReturnFalse_whenInvalidEmail(String email) {
        boolean result = Utility.isValidEmail(email);

        assertFalse(result);
    }

    @ParameterizedTest
    @NullAndEmptySource
//    @NullSource
//    @EmptySource
    @ValueSource(strings = {"email@gmail.de", "email@gmail.uk", "jsnsw.com"})
    void shouldReturnFalse_whenInvalidEmail_withEmpty(String email) {
        boolean result = Utility.isValidEmail(email);

        assertFalse(result);
    }

    @ParameterizedTest
    @EnumSource(CurrencyConverter.class)
    void shouldConverMoney_whenValidConverter(CurrencyConverter inputData) {
        double result = Utility.convertCurrency(inputData, 10.0);

        assertTrue(result > 0);
    }

    @ParameterizedTest
    @CsvSource({
            "49.2, USD, 10.0",
            "49.5, EUR, 10.0",
            "10.0, RON, 10.0",
    })
    void shouldConverMoney_whenValidConverter(double expectedResult, CurrencyConverter currency, double money) {
        double result = Utility.convertCurrency(currency, money);

       assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @CsvSource({
            "15,10,25",
            "100,10,110",
            "50,25,75"

    })
    void shouldAddNumbers(int x, int y, int expected) {
        int result = Utility.add(x,y);
        assertEquals(expected, result);

    }

    //    @Test
//    void shouldReturnTrue_whenValidEmail() {
//        boolean result = Utility.isValidEmail("email@gmail.com");
//
//        assertTrue(result);
//    }
//
//    @Test
//    void shouldReturnTrue_whenValidEmail_RoCase() {
//        boolean result = Utility.isValidEmail("email@gmail.ro");
//
//        assertTrue(result);
//    }
//
//    @Test
//    void shouldReturnTrue_whenValidEmail_FrCase() {
//        boolean result = Utility.isValidEmail("email@gmail.fr");
//
//        assertTrue(result);
//    }
}